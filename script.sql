CREATE TABLE tabAccess( 
    name VARCHAR(50), 
    api_key text, 
    url_host text
);

CREATE TABLE tabPerson( 
    name VARCHAR(50), 
    number_document VARCHAR(8), 
    person_name VARCHAR(250)
);

CREATE TABLE tabCompany( 
    name VARCHAR(50), 
    number_document VARCHAR(11), 
    company_name VARCHAR(250),
    company_address VARCHAR(250),
    status_taxpayer VARCHAR(15),
    condition_taxpayer VARCHAR(15),
    type_taxpayer VARCHAR(250)
);

rails g model Person number_document string person_name string

rails g scaffold Person numbeDocument:string name:string  paternalLastName:string maternalLastName:string

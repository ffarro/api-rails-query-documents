require 'net/http'
require 'uri'
require 'nokogiri'
require 'json'

class Person < ApplicationRecord

    def self.splash(dni)
        init = Time.now.strftime('%s%L').to_i
  
        script = "
                  function main(splash, args)
                    assert(splash:go(args.url))
                    assert(splash:wait(0.5))
                    
                    local form = splash:select('#buscar-por-dni')
                    local values = {
                      dni = #{dni}
                    }
                    assert(form:fill(values))
                    assert(form:submit())
  
                    while not splash:select('table') do
                      splash:wait(0.1)
                    end
                  
                    return {
                      html = splash:html(),
                    }
                  end
        "
        
        uri = URI.parse("http://localhost:8050/execute?") 
  
        request = Net::HTTP::Post.new(uri)
        request.content_type = "application/json"
        request.body = JSON.dump({
          "url" => 'https://eldni.com/pe/buscar-por-dni',
          "wait" => 1,
          "lua_source" => script,
        })
  
        req_options = {
          use_ssl: uri.scheme == "https",
        }
  
        response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
          http.request(request)
        end
    
        page    = Nokogiri::HTML(response.body)
        items   = page.css('table tbody tr td')

        diff    = Time.now.strftime('%s%L').to_i - init
  
        data  = {
            numbeDocument:    items[0].text,
            name:             items[1].text,
            paternalLastName: items[2].text,
            maternalLastName: items[3].text,
            diff:             "#{diff} ms"   
        }
  
        return data

    end
  
end
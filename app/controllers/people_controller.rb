class PeopleController < ApplicationController
  before_action :set_person, only: %i[ show edit update destroy ]

  # GET /people or /people.json
  def index
    @people = Person.all
  end


  def query_db 
    init = Time.now.strftime('%s%L').to_i
    
    person = Person.where(:numbeDocument => params[:numbeDocument]).first
    #person = Person.where(:numbeDocument => '41250411').first
    
    if person !=  nil
      
      diff    = Time.now.strftime('%s%L').to_i - init

      render json: {
        message:  "Resultados encontrados en BD",
        data:     person,
        diff:     "#{diff} ms"
      }

    else
      resul = Person.splash(params[:numbeDocument])

      if !resul.nil?
        @person = Person.create(
          numbeDocument:    resul[:numbeDocument],
          name:             resul[:name],
          paternalLastName: resul[:paternalLastName],         
          maternalLastName: resul[:maternalLastName]
        )
      end

      render json: {
        message: "Búsqueda con splash y guardado en base de datos",
        data: resul, 
        diff: resul[:diff]
      }

    end
  end

  # GET /people/1 or /people/1.json
  def show
  end

  # GET /people/new
  def new
    @person = Person.new
  end

  # GET /people/1/edit
  def edit
  end

  # POST /people or /people.json
  def create
    @person = Person.new(person_params)

    respond_to do |format|
      if @person.save
        format.html { redirect_to person_url(@person), notice: "Person was successfully created." }
        format.json { render :show, status: :created, location: @person }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /people/1 or /people/1.json
  def update
    respond_to do |format|
      if @person.update(person_params)
        format.html { redirect_to person_url(@person), notice: "Person was successfully updated." }
        format.json { render :show, status: :ok, location: @person }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /people/1 or /people/1.json
  def destroy
    @person.destroy

    respond_to do |format|
      format.html { redirect_to people_url, notice: "Person was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_person
      @person = Person.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def person_params
      params.require(:person).permit(:numbeDocument, :name, :paternalLastName, :maternalLastName)
    end
end
